apt update && apt upgrade -y

# Install rocm

apt install libnuma-dev
reboot

wget -q -O - http://repo.radeon.com/rocm/rocm.gpg.key | apt-key add -
echo 'deb [arch=amd64] http://repo.radeon.com/rocm/apt/debian/ xenial main' | tee /etc/apt/sources.list.d/rocm.list
apt update
apt install rocm-dkms
reboot

usermod -a -G video $LOGNAME
usermod -a -G render $LOGNAME
echo 'ADD_EXTRA_GROUPS=1' | tee -a /etc/adduser.conf
echo 'EXTRA_GROUPS=video' | tee -a /etc/adduser.conf
echo 'EXTRA_GROUPS=render' | tee -a /etc/adduser.conf
reboot


# Install rest

apt install -y network-manager-openvpn-gnome openssh-server git curl zsh vim apt-transport-https ca-certificates software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

apt update && apt install -y docker-ce docker-ce-cli containerd.io

curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose

ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

gpasswd -a user docker

mkdir /opt/jupyter && cd /opt/jupyter

git clone https://gitlab.com/none.team/jupyter-lab.git .

docker-compose build
docker-compose up

crontab -e
# 0 * * * * chown user:user -R /home/user/Public/notebooks /home/user/Public/data /home/user/Public/models

visudo
# user ALL=(ALL) NOPASSWD: ALL

mkdir .vim/colors -p
curl -o test/test/material.vim https://raw.githubusercontent.com/kaicataldo/material.vim/main/colors/material.vim
vim .vimrc
# set nu
# set expandtab
# set tabstop=4
# set shiftwidth=4
# set termguicolors
# set background=dark
# colorscheme material
# let g:material_theme_style = 'default'

sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
vim .zshrc
# DISABLE_AUTO_UPDATE="true"
# ZSH_DISABLE_COMPFIX="true"

chsh -s $(which zsh)









