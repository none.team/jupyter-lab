FROM python:latest

RUN wget -q -O - http://repo.radeon.com/rocm/apt/3.7/rocm.gpg.key | apt-key add - && \
    echo 'deb [arch=amd64] http://repo.radeon.com/rocm/apt/3.7/ xenial main' | tee /etc/apt/sources.list.d/rocm.list && \
    apt update && \
    DEBIAN_FRONTEND=noninteractive apt install -y rocm-libs hipcub miopen-hip cmake clang kmod libnuma-dev

RUN git clone https://github.com/ROCmSoftwarePlatform/rccl.git && \
    cd rccl && ./install.sh -i

ENV HCC_AMDGPU_TARGET=gfx803
ENV CMAKE_PREFIX_PATH=/opt/rocm/include/hsa:/opt/rocm

RUN git clone https://github.com/pytorch/pytorch.git && \
    cd pytorch && git submodule update --init --recursive && \
    pip install -r requirements.txt && \
    python tools/amd_build/build_amd.py && \
    USE_ROCM=1 MAX_JOBS=8 python setup.py install --user

# RUN git clone https://github.com/pytorch/vision.git && \
#     cd vision && python setup.py install

# RUN git clone https://github.com/facebookresearch/detectron2.git && \
#     cd detectron2 && python setup.py install

RUN apt install -y nodejs npm \
                   texlive-xetex \
                   texlive-fonts-recommended \
                   texlive-generic-recommended \
                   pandoc 

WORKDIR /usr/src/app

COPY requirements.txt jupyter_notebook_config.py ./
RUN pip install --no-cache-dir -r requirements.txt

CMD [ "jupyter", "lab" ]

